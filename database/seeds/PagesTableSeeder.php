<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title' => 'Notebooks',
                'slug' => 'Computers',
                'intro' => 'Now we start big sales',
                'content' => 'Our computers is better'
            ],
            [
                'title' => 'Mobile Phone',
                'slug' => 'Phone',
                'intro' => 'Now we start big sales in phone store',
                'content' => 'Our phones is better'
            ],
            [
                'title' => 'Laptop',
                'slug' => 'Notebook',
                'intro' => 'Now we start big sales in our notebook store',
                'content' => 'Our prices is perfect for you'
            ],
            [
                'title' => 'headphones',
                'slug' => 'accessories',
                'intro' => 'in our store we have big choice of accessories',
                'content' => 'in our store you can buy all what you whant'
            ],
            [
                'title' => 'Protection',
                'slug' => 'Protection devices',
                'intro' => 'Protect your device',
                'content' => 'In our store you can protect any your device with protective glass'
            ],
        ]);
    }
}
