<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name' => 'Brad Pitt',
                'email' => 'bradpitt@gmail.com',
                'phone' => 3806777777,
                'feedback' => 'good price'
            ],
            [
                'customer_name' => 'Will Smith',
                'email' => 'willSmith@gmail.com',
                'phone' => 3806222222,
                'feedback' => 'funny toy'
            ],
            [
                'customer_name' => 'Jackie Chan',
                'email' => 'JackieChan@gmail.com',
                'phone' => 342222222,
                'feedback' => 'i am not speaking english'
            ],
            [
                'customer_name' => 'Angelina Jolie',
                'email' => 'Angelinajolie@gmail.com',
                'phone' => 38062312312,
                'feedback' => 'perfect'
            ],
            [
                'customer_name' => 'Alla Pugacheva',
                'email' => 'Alla@gmail.com',
                'phone' => 3806711377,
                'feedback' => '1m rose'
            ],
        ]);
    }
}
