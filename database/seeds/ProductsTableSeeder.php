<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'note-book',
                'slug' => 'HP',
                'price' => 300,
                'description' => 'gaming version'
            ],
            [
                'title' => 'ipad',
                'slug' => 'Apple',
                'price' => 400,
                'description' => 'Home device'
            ],
            [
                'title' => 'watch',
                'slug' => 'Apple',
                'price' => 500,
                'description' => 'smart device'
            ],
            [
                'title' => 'camera',
                'slug' => 'sony',
                'price' => 200,
                'description' => 'recording device'
            ],
            [
                'title' => 'laptop',
                'slug' => 'Acer',
                'price' => 400,
                'description' => 'Notebook'
            ],
        ]);
    }
}
